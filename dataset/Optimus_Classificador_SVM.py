#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys, csv
import pandas as pd


# In[2]:


import matplotlib.pyplot as plt
import numpy as np
import seaborn
get_ipython().run_line_magic('matplotlib', 'inline')


# In[3]:


from sklearn.metrics import confusion_matrix
from sklearn import svm
from sklearn.utils.multiclass import unique_labels


# In[4]:


from sklearn.ensemble import RandomForestClassifier


# In[5]:


from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam
import keras.backend as K

from keras.utils import to_categorical


# In[6]:


ds_path = os.path.join('dataset', 'dataset_sentinel-2_20190803.CSV')


# In[10]:


ds = pd.read_csv(ds_path, delimiter=';') 


# In[11]:


ds.head()


# In[12]:


classes = list(ds['Classe'].unique())
print(classes)


# In[13]:


ds_class_0 = ds[ds.Classe == classes[0]]
ds_class_1 = ds[ds.Classe == classes[1]]
ds_class_2 = ds[ds.Classe == classes[2]]


# In[14]:


len_ds_class_0 = len(ds_class_0)
len_ds_class_1 = len(ds_class_1)
len_ds_class_2 = len(ds_class_2)

i_train = 0.8
i_test = 1-i_train


# In[19]:


ds_class_0_train = ds_class_0[0:int(i_train*len_ds_class_0)]
ds_class_1_train = ds_class_1[0:int(i_train*len_ds_class_1)]
ds_class_2_train = ds_class_2[0:int(i_train*len_ds_class_2)]

ds_class_0_test = ds_class_0[int(i_train*len_ds_class_0):len_ds_class_0]
ds_class_1_test = ds_class_1[int(i_train*len_ds_class_1):len_ds_class_1]
ds_class_2_test = ds_class_2[int(i_train*len_ds_class_2):len_ds_class_2]


# In[24]:


# Preparar dados para Algorítimo
X_train = [ds_class_0_train, ds_class_1_train, ds_class_2_train]
X_train = pd.concat(X_train)
y_train = X_train['Classe']
X_train = X_train.drop(['Classe'],axis=1)
X_train = np.asarray(X_train)
y_train = np.asarray(y_train)

X_test = [ds_class_0_test, ds_class_1_test, ds_class_2_test]
X_test = pd.concat(X_test)
y_test = X_test['Classe']
X_test = X_test.drop(['Classe'],axis=1)
X_test = np.asarray(X_test)
y_test = np.asarray(y_test)


# ### SVM

# In[25]:


classifier = svm.SVC(kernel='linear', C=0.01)
classifier.fit(X_train, y_train)
prediction_SVM = classifier.predict(X_test)


# In[26]:


classifier.score(X_test,y_test)


# In[27]:


confusion_matrix(y_test, prediction_SVM)


# ### Random forest with scikit-learn

# In[28]:


random_forest = RandomForestClassifier(n_estimators=15,)
random_forest.fit(X_train,y_train)
random_forest.score(X_test,y_test)


# In[29]:


prediction_RF = random_forest.predict(X_test)


# In[30]:


confusion_matrix(y_test, prediction_RF)


# In[31]:


random_forest.score(X_test,y_test)


# ### Neural Network

# In[34]:


def model():
    global network_history
    model = Sequential()
    model.add(Dense(64,input_shape=(16,)))
    model.add(Dropout(0.2))
    model.add(Dense(64, kernel_initializer='normal'))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='mean_squared_error', optimizer=Adam(lr=0.0001), metrics=['accuracy'])
    
    network_history = model.fit(X_train, y_train, batch_size=2, epochs=300, verbose=1)
    return model


# In[35]:


to_categorical(y_train)


# In[36]:


model = model()


# In[ ]:





# In[ ]:




