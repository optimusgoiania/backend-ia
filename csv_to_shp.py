from sys import argv

script, input_file, EPSG_code, delimiter, export_shp = argv

import csv
import osgeo.ogr, osgeo.osr #we will need some packages
from osgeo import ogr #and one more for the creation of a new field

spatialReference = osgeo.osr.SpatialReference() #will create a spatial reference locally to tell the system what the reference will be
spatialReference.ImportFromEPSG(int(EPSG_code)) #here we define this reference to be utm Zone 48N with wgs84..
driver = osgeo.ogr.GetDriverByName('ESRI Shapefile') # will select the driver foir our shp-file creation.
shapeData = driver.CreateDataSource(export_shp) #so there we will store our data
layer = shapeData.CreateLayer('layer', spatialReference, osgeo.ogr.wkbPoint) #this will create a corresponding layer for our data with given spatial information.
layer_defn = layer.GetLayerDefn() # gets parameters of the current shapefile
index = 0

with open(input_file, 'rb') as csvfile:
	readerDict = csv.DictReader(csvfile, delimiter=delimiter)
	for field in readerDict.fieldnames:
		new_field = ogr.FieldDefn(field, ogr.OFTString) #we will create a new field called Hometown as String
		layer.CreateField(new_field)
	for row in readerDict:
		print(row['LAT'], row['LON'])
		point = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)
		point.AddPoint(float(row['LON']), float(row['LAT'])) #we do have LATs and LONs as Strings, so we convert them
		feature = osgeo.ogr.Feature(layer_defn) 
		feature.SetGeometry(point) #set the coordinates
		feature.SetFID(index)
		for field in readerDict.fieldnames:
			i = feature.GetFieldIndex(field)
			feature.SetField(i, row[field])
		layer.CreateFeature(feature)
		index += 1
shapeData.Destroy() #lets close the shapefile
