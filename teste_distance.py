from distance import Distance
import os


def test_distance():
    apa_shp_file = os.path.join('shp', 'apa.shp', 'APA.shp')
    lat = -16.6825472
    lon = -49.2765589
    dist = Distance()
    distance = dist.menor_distancia_shape(lat, lon, apa_shp_file)
    print(distance)


if __name__ == "__main__":
    test_distance()
