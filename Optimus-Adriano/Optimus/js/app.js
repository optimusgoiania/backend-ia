// Variaveis Globais
var oMapa = {};
var aDataDisponivel = [];
var aMarcadores = [];
var aDados = [];
var oClusterMarkers;

//
var fInitPage = function()
{
    fInitMap();
    fLoadInitialData();
};

// 
var fPrepFormFiltro = function()
{
    $('form[name=filtro]').on('submit', fReloadData);
}

//
var fInitMap = function()
{
    var aMapaPosicaoInicial = [-16.680243, -49.256365];
    var iMapaZoomInicial = 13;

    oMapa = L.map('MapaPrincipal').setView(aMapaPosicaoInicial, iMapaZoomInicial);
    L.tileLayer('http://192.168.0.64/osm/{z}/{x}/{y}.png',
    {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 18
    }).addTo(oMapa);
};

//
var fLoadInitialData = function()
{

    $.get('dados/entulho.json', function(oResponse)
    {
        aDados = oResponse;
        fReloadData();

    }, 'json').fail(function()
    {
        aDados = [];
    });

};

//
var fReloadData = function()
{

    // Remover marcadores existentes
    $.each(aMarcadores, function(iIdx, oItem)
    {
        oItem.removeFrom(oMapa);
    });

    // Cluster
    oClusterMarkers = L.markerClusterGroup();

    // Mapeando os valores
    $.map(aDados, function(oItem)
    {

        if (moment(oItem.image_generation_time).isValid())
        {
            var iTimestamp = moment(oItem.image_generation_time).unix();
            if ($.inArray(iTimestamp, aDataDisponivel) == -1) aDataDisponivel.push(iTimestamp);
        }

        var oPosicao = L.latLng(oItem.latitude, oItem.longitude);
        var oMarcador = L.marker(oPosicao, {
            title: '',
            riseOnHover: true
        });

        oClusterMarkers.addLayer(oMarcador);

    });

    oMapa.addLayer(oClusterMarkers);

    $.each(aDataDisponivel, function(iIdx, iTimestamp)
    {

        var oData = moment.unix(iTimestamp);

        var eOption = $('<option/>');
        eOption.val(iTimestamp);
        eOption.html(oData.format('DD/MM/YYYY'));
        eOption.appendTo($('select[name=qryFiltroData]'));

    });
};

// Inicialização
$(document).ready(fInitPage);
