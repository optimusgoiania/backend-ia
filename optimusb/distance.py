from osgeo import ogr
import os
import osgeo.osr
import glob


class Distance:

    def __init__(self, lat=-16.6825472, lon=-49.2765589, shp_file=None):
        self.lat = lat
        self.lon = lon
        self.shp_file = shp_file

    # Cria Ponto (shp)
    def point2shp(self, lat, lon):
        # retorna caminho para o Shapefile
        index = 0
        spatial_reference = osgeo.osr.SpatialReference()
        spatial_reference.ImportFromEPSG(int(4326))
        driver = osgeo.ogr.GetDriverByName('ESRI Shapefile')
        tmp = os.path.join('tmp', 'export.shp')
        shape_data = driver.CreateDataSource(tmp)
        layer = shape_data.CreateLayer('layer', spatial_reference, osgeo.ogr.wkbPoint)
        layer_defn = layer.GetLayerDefn()  # gets parameters of the current shapefile
        point = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)
        point.AddPoint(float(lon), float(lat))
        feature = osgeo.ogr.Feature(layer_defn)
        feature.SetGeometry(point)
        feature.SetFID(index)
        layer.CreateFeature(feature)
        shape_data.Destroy()
        return tmp

    # Calcula distância entre Ponto (shp) e Shape (shp)
    def menor_distancia_shape(self, lat, lon, area_shp_file):
        point_shp_file = self.point2shp(lat, lon)
        driver = ogr.GetDriverByName('ESRI Shapefile')
        area_shp = driver.Open(area_shp_file, 0)
        area_shp_layer = area_shp.GetLayer()
        area_shp_feat = area_shp_layer.GetNextFeature()
        point_shp = driver.Open(point_shp_file, 0)
        point_shp_layer = point_shp.GetLayer()
        point_shp_feat = point_shp_layer.GetNextFeature()
        point_shp_geom = point_shp_feat.GetGeometryRef()
        distlist = []
        menor_distancia = 999999999
        while area_shp_feat:
            area_shp_geom = area_shp_feat.GetGeometryRef()
            dist = point_shp_geom.Distance(area_shp_geom)
            distlist.append(dist * 100000)  ## retorno em metros
            if dist * 100000 < menor_distancia:
                menor_distancia = dist * 100000
            area_shp_feat.Destroy()
            area_shp_feat = area_shp_layer.GetNextFeature()
        point_shp = ''
        self.del_tmp_files(point_shp_file)
        return menor_distancia

    # Para não ficar lixo, apaga arquivos temporários utilizados
    def del_tmp_files(self, point_shp_file):
        p = point_shp_file
        p = p.split(".")[0]
        p = str(p).split("\\")
        try:
            files = glob.glob(p[0] + "\\*")
            for f in files:
                os.remove(f)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))
