from django.db import models
from django.contrib.gis.geos import GEOSGeometry
from django.core.serializers import serialize

# Create your models here.
from django.contrib.gis.db import models as geomodels
from shapely.geometry import Polygon


class Shape(models.Model):
    name = models.CharField(max_length=100, blank=False)
    geometry = geomodels.GeometryField(srid=4326)

    class Meta:
        # order of drop-down list items
        ordering = ('name',)

        # plural form in admin view
        verbose_name_plural = 'shapes'

    def __str__(self):
        return self.name

    @property
    def shape_coords_list(self):
        return GEOSGeometry(self.geometry).json
