from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    # Shape detail view
    path('shape/<int:pk>/', views.ShapesDetailView.as_view(), name='shape-detail'),
    path('shape/gyn/', views.shapeGyn, name='shape_syn'),
    path('shape/apa/', views.shapeAPA, name='shape_APA'),
    path('shape/ara/', views.shapeARA, name='shape_ARA'),
    path('shape/app/', views.shapeAPP, name='shape_APP'),
]
