from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis import geos
import json
from django.db import connection

# Create your views here.

from django.views.generic import DetailView
from .models import Shape


class ShapesDetailView(DetailView):
    """
        Shape detail view.
    """
    template_name = 'shapes/shape_detail.html'
    model = Shape


def shapeGyn(request):
    for e in Shape.objects.filter(id=2):
        result = GEOSGeometry(e.geometry).json
    return JsonResponse({'data': result})

def shapeAPA(request):
    sql = "SELECT '31' as id, 'APA' as name, st_multi(st_union(geometry)) as geometry from shapes_shape shp WHERE shp.name like '0631%'"
    with connection.cursor() as cursor:
        cursor.execute(sql)
        row = cursor.fetchone()
    result = GEOSGeometry(row[2]).json
    return JsonResponse({'data': result})

def shapeARA(request):
    sql = "SELECT '30' as id, 'ARA' as name, st_multi(st_union(geometry)) as geometry from shapes_shape shp WHERE shp.name like '0630%'"
    with connection.cursor() as cursor:
        cursor.execute(sql)
        row = cursor.fetchone()
    result = GEOSGeometry(row[2]).json
    return JsonResponse({'data': result})

def shapeAPP(request):
    sql = "SELECT '33' as id, 'ARA' as name, st_collect((geometry)) as geometry from shapes_shape shp WHERE shp.name like '0633%'"
    with connection.cursor() as cursor:
        cursor.execute(sql)
        row = cursor.fetchone()
    result = GEOSGeometry(row[2]).json
    return JsonResponse({'data': result})

