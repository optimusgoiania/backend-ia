import json
import os
import urllib.request

import geojson
import pandas as pd
from django.http import JsonResponse
from django.shortcuts import render


# Create your views here.
def index(request):
    # entulhos = get_entulho()
    shape_gyn = get_shape_gyn(request)
    shape_apa = get_shape_apa(request)
    shape_ara = get_shape_ara(request)
    shape_app = get_shape_app(request)
    rcc = get_rcc(request)
    result = {'Goiania': shape_gyn, 'APA': shape_apa, 'ARA': shape_ara, 'APP': shape_app, 'RCC': rcc}
    return render(request, 'dashboard/index.html', {'resultado': result})


def index_json(request):
    context = {}
    entulhos = get_rcc(request)
    return JsonResponse(entulhos)

def get_shape_gyn(request):
    url = 'http://127.0.0.1:' + request.META['SERVER_PORT'] + '/shapes/shape/gyn/'
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    return data

def get_shape_apa(request):
    url = 'http://127.0.0.1:' + request.META['SERVER_PORT'] + '/shapes/shape/apa/'
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    return data

def get_shape_ara(request):
    url = 'http://127.0.0.1:' + request.META['SERVER_PORT'] + '/shapes/shape/ara/'
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    return data

def get_shape_app(request):
    url = 'http://127.0.0.1:' + request.META['SERVER_PORT'] + '/shapes/shape/app/'
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    return data

def get_rcc(request):
    url = 'http://127.0.0.1:' + request.META['SERVER_PORT'] + '/processa/1/'
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    return data



def data2geojson(df):
    features = []
    insert_features = lambda X: features.append(
        geojson.Feature(geometry=geojson.Point((X["longitude"],
                                                X["latitude"])),
                        properties=dict(name=X["descricao"],
                                        description=X["descricao"])
                        )
    )
    df.apply(insert_features, axis=1)
    return geojson.FeatureCollection(features)
