from django.urls import path
from djgeojson.views import GeoJSONLayerView
from django.urls import include

from . import views
urlpatterns = [
    path('', views.index, name='index'),
    #path('data.geojson', views.index_json, name='data_json'),
    #path('data', GeoJSONLayerView(model=views.index_json), name='data'),
]
