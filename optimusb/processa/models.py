from django.db import models

# Create your models here.

from django.db import models
from django.contrib.gis.geos import GEOSGeometry
from django.core.serializers import serialize

# Create your models here.
from django.contrib.gis.db import models as geomodels
from shapely.geometry import Polygon


class RCC(models.Model):
    descricao = models.CharField(max_length=60)
    band_b2 = models.IntegerField()
    band_b3 = models.IntegerField()
    band_b4 = models.IntegerField()
    band_b8 = models.IntegerField()
    image_index = models.CharField(max_length=120)
    imagemid = models.CharField(max_length=120)
    image_reflectance = models.DecimalField(max_digits=20, decimal_places=14)
    image_cloudy_percentage = models.DecimalField(max_digits=10, decimal_places=6)
    image_generation_time = models.BigIntegerField()
    image_spacecraft_name = models.CharField(max_length=60)
    distancia_apa = models.DecimalField(max_digits=22, decimal_places=16)
    distancia_app = models.DecimalField(max_digits=22, decimal_places=16)
    distancia_ara = models.DecimalField(max_digits=22, decimal_places=16)
    data_processamento = models.DateTimeField()
    coordenada = geomodels.PointField(srid=4326)

#
    class Meta:
         # order of drop-down list items
         ordering = ('image_generation_time',)

         # plural form in admin view
         verbose_name_plural = "RCCs"

    def __str__(self):
        return self.descricao
#
#     @property
#     def shape_coords_list(self):
#         return GEOSGeometry(self.geometry).json
#
