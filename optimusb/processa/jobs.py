from django.conf import settings
from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore, register_events, register_job
from .sentinel2 import Sentinel2
import logging

logging.basicConfig()
logging.getLogger('apscheduler').setLevel(logging.DEBUG)

scheduler = BackgroundScheduler()
scheduler.add_jobstore(DjangoJobStore(), "default")

hora = getattr(settings, "EXEC_HORA", None)
minuto = getattr(settings, "EXEC_MINUTO", None)


@register_job(scheduler, "cron", hour=hora, minute=minuto, replace_existing=False, id='processa_sentinel2')
def processa_sentinel():
    print("Iniciando processamento do Sentinel-2")
    logging.getLogger('info_logger').info('Iniciando processamento do Sentinel-2')
    sat = Sentinel2()
    r = sat.processa_imagens_satelite()
    print(r)


register_events(scheduler)
scheduler.start()
logging.getLogger('info_logger').info('Scheduler started!')
print('Scheduler started!')
