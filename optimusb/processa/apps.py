from django.apps import AppConfig
from django.conf import settings


class ProcessaConfig(AppConfig):
    name = 'processa'
