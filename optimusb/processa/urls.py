from django.urls import path

from . import views
from . import jobs
from djgeojson.views import GeoJSONLayerView
from .models import RCC

urlpatterns = {
    # path('imagem_satelite/', views.processa_imagem_satelite, name='processa_imagem_satelite'),
    # path('<int:distancia>/', views.ultima_imagem_distancia, name='ultima_imagem_distancia'),
    path('lista_distinct_image_generation_time/', views.lista_distinct_image_generation_time, name='processa_lista_datas'),
    path('data/<int:data>/', views.get_coordenadas_por_data, name='get_coordenadas_por_data'),
    path('data/<int:data>/<int:distancia>/', views.get_coordenadas_por_data_por_distancia, name='get_coordenadas_por_data_por_distancia'),
    path('data/<int:data>/<int:distancia>/q/', views.get_coordenadas_por_data_por_distancia_quantidade, name='get_coordenadas_por_data_por_distancia_quantidade'),
    # path('hist_coordenada/<int:lat_lon>/', views.get_historico_bandas_lat_lon, name='get_historico_bandas_lat_lon'),
    path('historico_coordenada/<str:lat_lon>/', views.get_historico_bandas_lat_lon, name='get_historico_bandas_lat_lon'),
    path('historico_coordenada/<str:lat_lon>/<int:data>/', views.get_historico_bandas_lat_lon_data, name='get_historico_bandas_lat_lon_data'),
    path('historico_coordenada_seentulho/<str:lat_lon>/<int:data>/', views.get_historico_bandas_lat_lon_data_seentulho, name='get_historico_bandas_lat_lon_data_seentulho'),
}
