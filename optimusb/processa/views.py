import sys

from django.core.serializers import serialize
from django.shortcuts import render
from django.http import HttpResponse

from .sentinel2 import Sentinel2
from .models import RCC
from django.db.models import Q
from django.db.models import Max
from django.db import connection
import time as t
from django.contrib.gis.geos import GEOSGeometry
from django.http import JsonResponse
import json
from django.core.serializers.json import DjangoJSONEncoder
from datetime import datetime, timedelta, date, time
from django.contrib.gis.geos import Point
import numpy as np
import ee


# Create your views here.

def processa_imagem_satelite(request):
    sat = Sentinel2()
    resultado = sat.processa_imagens_satelite()
    return HttpResponse("Processamento concluído! Veja erros no arquivo de log.")


def ultima_imagem_distancia(request, distancia):
    max_date = RCC.objects.aggregate(Max('image_generation_time'))['image_generation_time__max']
    rcc = RCC.objects.filter(
        (Q(distancia_app__lte=distancia) | Q(distancia_ara__lte=distancia) | Q(distancia_apa__lte=distancia)) & Q(
            image_generation_time=max_date))
    rcc_list = list(rcc.values())
    lista = []
    for rl in rcc_list:
        lista.append(GEOSGeometry(rl['coordenada']).json)

    r = serialize('geojson', rcc, geometry_field='point', fields=('coordenada',))
    return JsonResponse({'data': r})
    # return JsonResponse({'data': (', '.join(lista))})
    # return JsonResponse({'data': lista})


def lista_distinct_image_generation_time(request):
    rcc = RCC.objects.values('image_generation_time').distinct()
    lista_distinct_image_generation_time = list(rcc.values('image_generation_time'))
    # lista = [datetime.utcfromtimestamp(int(l['image_generation_time']/1000)).strftime('%d/%m/%Y') for l in lista_distinct_image_generation_time]
    lista = [int(l['image_generation_time']) for l in lista_distinct_image_generation_time]
    return JsonResponse({'lista_distinct_image_generation_time': lista})


def get_coordenadas_por_data(request, data):
    data = data
    rcc = RCC.objects.filter((Q(image_generation_time=data)))
    rcc_list = list(rcc.values())
    rcc_l = list(rcc.values('coordenada'))

    coordenadas_rcc = []

    for c in rcc_l:
        lat = GEOSGeometry(c['coordenada']).centroid.y
        lon = GEOSGeometry(c['coordenada']).centroid.x
        coordenada = [lon, lat]
        coordenadas_rcc.append(coordenada)

    return JsonResponse({'coordenadas_rcc': coordenadas_rcc})


def get_coordenadas_por_data_por_distancia(request, data, distancia):
    data = data
    distancia = distancia

    # rcc = RCC.objects.filter(
    #     (Q(distancia_app__lte=distancia) | Q(distancia_ara__lte=distancia) | Q(distancia_apa__lte=distancia)) & Q(
    #         image_generation_time=data))

    dia = 60 * 60 * 24 * 1000
    dia_cedo = data - dia
    dia_tarde = data + dia

    print("data    : ", data)
    print("dia_cedo: ", dia_cedo)
    print("dia_tarde: ", dia_tarde)

    rcc = RCC.objects.filter(
        (Q(distancia_app__lte=distancia) | Q(distancia_ara__lte=distancia) | Q(distancia_apa__lte=distancia)) & (Q(
            image_generation_time__gte=dia_cedo) & Q(image_generation_time__lte=dia_tarde)))

    rcc_l = list(rcc.values('coordenada'))

    coordenadas_rcc = []

    for c in rcc_l:
        lat = GEOSGeometry(c['coordenada']).centroid.y
        lon = GEOSGeometry(c['coordenada']).centroid.x
        coordenada = [lon, lat]
        coordenadas_rcc.append(coordenada)

    return JsonResponse({'coordenadas_rcc_data_distancia': coordenadas_rcc})

def get_coordenadas_por_data_por_distancia_quantidade(request, data, distancia):
    data = data
    distancia = distancia

    # rcc = RCC.objects.filter(
    #     (Q(distancia_app__lte=distancia) | Q(distancia_ara__lte=distancia) | Q(distancia_apa__lte=distancia)) & Q(
    #         image_generation_time=data))

    dia = 60 * 60 * 24 * 1000
    dia_cedo = data - dia
    dia_tarde = data + dia

    print("data    : ", data)
    print("dia_cedo: ", dia_cedo)
    print("dia_tarde: ", dia_tarde)

    rcc = RCC.objects.filter(
        (Q(distancia_app__lte=distancia) | Q(distancia_ara__lte=distancia) | Q(distancia_apa__lte=distancia)) & (Q(
            image_generation_time__gte=dia_cedo) & Q(image_generation_time__lte=dia_tarde)))

    rcc_l = list(rcc.values('coordenada'))


    coordenadas_rcc = []

    for c in rcc_l:
        lat = GEOSGeometry(c['coordenada']).centroid.y
        lon = GEOSGeometry(c['coordenada']).centroid.x
        coordenada = [lon, lat]

        data_par = datetime.fromtimestamp(data/1000)
        dia45 = data_par + timedelta(days=-45)
        dia45 = t.mktime(dia45.timetuple())
        dia45 = dia45*1000

        coord = Point(float(lon), float(lat), srid=4326)
        rcc_seentulho = RCC.objects.filter((Q(
            image_generation_time__gte=dia45) & Q(image_generation_time__lte=data)) & Q(coordenada=c['coordenada']))
        rcc_l = list(rcc_seentulho)

        if len(rcc_l) > 2:
            print(coordenada, len(rcc_l))

        coordenadas_rcc.append([coordenada, len(rcc_l)])

    return JsonResponse({'coordenadas_rcc_data_distancia': coordenadas_rcc})


def get_historico_bandas_lat_lon(request, lat_lon):
    lat, lon = lat_lon.split(",")

    latitude = float(lat)
    longitude = float(lon)
    # pnt = Point((longitude, latitude), srid=4326)
    # print(pnt)
    # # pnt = Point((longitude, latitude), srid=4326)
    # rcc = RCC.objects.filter(Q(coordenada=pnt))
    # rcc_l = list(rcc.values('coordenada', 'image_generation_time', 'band_b2', 'band_b3', 'band_b4', 'band_b8'))
    #
    # list_rcc = {"list_rcc": []}
    # for r in rcc_l:
    #     rcc = {}
    #     latitude = GEOSGeometry(r['coordenada']).centroid.y
    #     longitude = GEOSGeometry(r['coordenada']).centroid.x
    #     rcc['coordenada'] = [latitude, longitude]
    #     rcc['image_generation_time'] = r['image_generation_time']
    #     rcc['band_b2'] = r['band_b2']
    #     rcc['band_b3'] = r['band_b3']
    #     rcc['band_b4'] = r['band_b4']
    #     rcc['band_b8'] = r['band_b8']
    #     list_rcc["list_rcc"].append(rcc)

    try:
        ee.Initialize()
        print('The Earth Engine package initialized successfully!')
    except ee.EEException as e:
        print('The Earth Engine package failed to initialize!')
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    ## Import GEE Feature Collection
    gyn = ee.FeatureCollection('ft:1RTplyVBpfKnE4lJg_nRZaXL6y-_e1jgE17Q8fwhC')
    cloudiness = 5;  ## cloudiness in %
    now_str = str(datetime.now().strftime("%Y-%m-%d"))
    now_30 = str((datetime.now() - timedelta(days=45)).strftime("%Y-%m-%d"))
    collection = ee.ImageCollection('COPERNICUS/S2').filterDate(now_30, now_str).filterBounds(gyn).filter(
        ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', cloudiness)).sort('GENERATION_TIME', True).filter(
        ee.Filter.eq('MGRS_TILE', '22KFG'))
    collectionList = collection.toList(collection.size())
    collectionSize = collectionList.size().getInfo()
    list_rcc = {"list_rcc": []}
    for i in range(collectionSize):
        rcc = {}
        image = ee.Image(collectionList.get(i))
        print(image.getInfo()['id'], image.get('SPACECRAFT_NAME').getInfo())
        image = ee.Image(image.getInfo()['id'])
        IMAGE_GENERATION_TIME = image.get('GENERATION_TIME').getInfo()
        coordenada = [longitude, latitude]
        point = ee.Geometry.Point(coordenada)
        data = image.reduceRegion(ee.Reducer.first(), point, 10)
        dataN = ee.Number(data)
        dict = ee.Dictionary(dataN)
        b2 = ee.Array(dict.get('B2').getInfo()).getInfo()
        b3 = ee.Array(dict.get('B3').getInfo()).getInfo()
        b4 = ee.Array(dict.get('B4').getInfo()).getInfo()
        b8 = ee.Array(dict.get('B8').getInfo()).getInfo()

        print(IMAGE_GENERATION_TIME, b2, b3, b4, b8)
        rcc['coordenada'] = [latitude, longitude]
        rcc['image_generation_time'] = IMAGE_GENERATION_TIME
        rcc['band_b2'] = b2
        rcc['band_b3'] = b3
        rcc['band_b4'] = b4
        rcc['band_b8'] = b8
        list_rcc["list_rcc"].append(rcc)

    return JsonResponse({'historico_coordenada': list_rcc})


def get_historico_bandas_lat_lon_data(request, lat_lon, data):
    lat, lon = lat_lon.split(",")
    data = data

    latitude = float(lat)
    longitude = float(lon)

    try:
        ee.Initialize()
        print('The Earth Engine package initialized successfully!')
    except ee.EEException as e:
        print('The Earth Engine package failed to initialize!')
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    ## Import GEE Feature Collection
    gyn = ee.FeatureCollection('ft:1RTplyVBpfKnE4lJg_nRZaXL6y-_e1jgE17Q8fwhC')
    cloudiness = 5  ## cloudiness in %
    now_str = str(datetime.utcfromtimestamp(data / 1000).strftime('%Y-%m-%d'))
    data_30 = ((datetime.utcfromtimestamp(data / 1000)) - timedelta(days=45))
    now_30 = str(data_30.strftime('%Y-%m-%d'))
    collection = ee.ImageCollection('COPERNICUS/S2').filterDate(now_30, now_str).filterBounds(gyn).filter(
        ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', cloudiness)).sort('GENERATION_TIME', True).filter(
        ee.Filter.eq('MGRS_TILE', '22KFG'))
    collectionList = collection.toList(collection.size())
    collectionSize = collectionList.size().getInfo()
    list_rcc = {"list_rcc": []}
    for i in range(collectionSize):
        rcc = {}
        image = ee.Image(collectionList.get(i))
        print(image.getInfo()['id'], image.get('SPACECRAFT_NAME').getInfo())
        image = ee.Image(image.getInfo()['id'])
        IMAGE_GENERATION_TIME = image.get('GENERATION_TIME').getInfo()
        coordenada = [longitude, latitude]
        point = ee.Geometry.Point(coordenada)
        data = image.reduceRegion(ee.Reducer.first(), point, 10)
        dataN = ee.Number(data)
        dict = ee.Dictionary(dataN)
        b2 = ee.Array(dict.get('B2').getInfo()).getInfo()
        b3 = ee.Array(dict.get('B3').getInfo()).getInfo()
        b4 = ee.Array(dict.get('B4').getInfo()).getInfo()
        b8 = ee.Array(dict.get('B8').getInfo()).getInfo()

        print(IMAGE_GENERATION_TIME, b2, b3, b4, b8)
        rcc['coordenada'] = [latitude, longitude]
        rcc['image_generation_time'] = IMAGE_GENERATION_TIME
        rcc['band_b2'] = b2
        rcc['band_b3'] = b3
        rcc['band_b4'] = b4
        rcc['band_b8'] = b8
        list_rcc["list_rcc"].append(rcc)

    return JsonResponse({'historico_coordenada': list_rcc})

def get_historico_bandas_lat_lon_data_seentulho(request, lat_lon, data):
    lat, lon = lat_lon.split(",")
    image_generation_time = data

    latitude = float(lat)
    longitude = float(lon)

    try:
        ee.Initialize()
        print('The Earth Engine package initialized successfully!')
    except ee.EEException as e:
        print('The Earth Engine package failed to initialize!')
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    ## Import GEE Feature Collection
    gyn = ee.FeatureCollection('ft:1RTplyVBpfKnE4lJg_nRZaXL6y-_e1jgE17Q8fwhC')
    cloudiness = 5  ## cloudiness in %
    now_str = str(datetime.utcfromtimestamp(image_generation_time / 1000).strftime('%Y-%m-%d'))
    data_30 = ((datetime.utcfromtimestamp(image_generation_time / 1000)) - timedelta(days=45))
    now_30 = str(data_30.strftime('%Y-%m-%d'))
    collection = ee.ImageCollection('COPERNICUS/S2').filterDate(now_30, now_str).filterBounds(gyn).filter(
        ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', cloudiness)).sort('GENERATION_TIME', True).filter(
        ee.Filter.eq('MGRS_TILE', '22KFG'))
    collectionList = collection.toList(collection.size())
    collectionSize = collectionList.size().getInfo()
    list_rcc = {"list_rcc": []}
    for i in range(collectionSize):
        rcc = {}
        image = ee.Image(collectionList.get(i))
        print(image.getInfo()['id'], image.get('SPACECRAFT_NAME').getInfo())
        image = ee.Image(image.getInfo()['id'])
        IMAGE_GENERATION_TIME = image.get('GENERATION_TIME').getInfo()
        coordenada = [longitude, latitude]
        point = ee.Geometry.Point(coordenada)
        data = image.reduceRegion(ee.Reducer.first(), point, 10)
        dataN = ee.Number(data)
        dict = ee.Dictionary(dataN)
        b2 = ee.Array(dict.get('B2').getInfo()).getInfo()
        b3 = ee.Array(dict.get('B3').getInfo()).getInfo()
        b4 = ee.Array(dict.get('B4').getInfo()).getInfo()
        b8 = ee.Array(dict.get('B8').getInfo()).getInfo()

        # Buscar se nesta IMAGE_GENERATION_TIME o sistema identificou presença de entulho:
        # Fazer a consulta em banco de dados pela coordenada e pela IMAGE_GENERATION_TIME

        coord = Point(float(lon), float(lat), srid=4326)
        rcc_seentulho = RCC.objects.filter(Q(image_generation_time=str(IMAGE_GENERATION_TIME)) & Q(coordenada=coord))
        print("query: ", rcc_seentulho.query)
        # print("data: ", image_generation_time, type(image_generation_time))
        # rcc = RCC.objects.filter(image_generation_time=str(image_generation_time))
        # rcc_l = list(rcc.values('coordenada'))
        rcc_l = list(rcc_seentulho)
        print("rcc_l: ", rcc_l)
        if (len(rcc_l) > 0):
            is_entulho = 1
        else:
            is_entulho = 0

        print(IMAGE_GENERATION_TIME, b2, b3, b4, b8)
        rcc['coordenada'] = [latitude, longitude]
        rcc['image_generation_time'] = IMAGE_GENERATION_TIME
        rcc['band_b2'] = b2
        rcc['band_b3'] = b3
        rcc['band_b4'] = b4
        rcc['band_b8'] = b8
        rcc['is_entulho'] = is_entulho
        list_rcc["list_rcc"].append(rcc)

    return JsonResponse({'historico_coordenada': list_rcc})
