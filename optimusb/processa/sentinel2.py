import ee
from datetime import datetime, timedelta
import pandas as pd
import sys
import joblib
import numpy as np
import os
from tqdm import tqdm
from distance import Distance
from .models import RCC
from django.contrib.gis.geos import Point
from django.contrib.gis.geos import GEOSGeometry
import pytz

import logging

logging.basicConfig()


class Sentinel2:

    def __init__(self):
        logging.getLogger('info_logger').info('Classe instanciada!')

    def processa_imagens_satelite(self):
        try:
            ee.Initialize()
            logging.getLogger('info_logger').info('The Earth Engine package initialized successfully!')
        except ee.EEException as e:
            # print('The Earth Engine package failed to initialize!')
            logging.getLogger('error_logger').error(repr(e))
            logging.getLogger('error_logger').error(r'The Earth Engine package failed to initialize!')
        except:
            # print("Unexpected error:", sys.exc_info()[0])
            logging.getLogger('error_logger').error("Unexpected error:", sys.exc_info()[0])
            raise

        ## Import GEE Feature Collection
        try:
            logging.getLogger('info_logger').info(r'Importando shape gyn!')
            gyn = ee.FeatureCollection('ft:1RTplyVBpfKnE4lJg_nRZaXL6y-_e1jgE17Q8fwhC')
        except ee.EEException as e:
            logging.getLogger('error_logger').error(repr(e))
            logging.getLogger('error_logger').error(r'Import FeatureCollection Error (gyn)')
            raise

        # Define the URL format used for Earth Engine generated map tiles.
        EE_TILES = 'https://earthengine.googleapis.com/map/{mapid}/{{z}}/{{x}}/{{y}}?token={token}'

        cloudiness = 5;  ## cloudiness in %
        now_str = str(datetime.now().strftime("%Y-%m-%d"))
        now_30 = str((datetime.now() - timedelta(days=60)).strftime("%Y-%m-%d"))

        bands = ['B4', 'B3', 'B2']

        collection = ee.ImageCollection('COPERNICUS/S2').filterDate(now_30, now_str).filterBounds(gyn).filter(
            ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', cloudiness)).sort('GENERATION_TIME', True).filter(
            ee.Filter.eq('MGRS_TILE', '22KFG'))

        collectionList = collection.toList(collection.size())
        collectionSize = collectionList.size().getInfo()

        print("Verificando imagens processadas...")

        lista_imagens_processadas = []
        arquivo_lista_imagens_processadas = 'lista_imagens_processadas.dat'
        if os.path.exists(arquivo_lista_imagens_processadas):
            f = open(arquivo_lista_imagens_processadas, "r")
            line = f.readline().rstrip()
            while line:
                lista_imagens_processadas.append(line)
                line = f.readline().rstrip()
            f.close()
        else:
            file = open(arquivo_lista_imagens_processadas, 'w+')
            file.close()
        logging.getLogger('info_logger').info(repr(("Lista de Imagens processadas: ", lista_imagens_processadas)))

        logging.getLogger('info_logger').info("Iniciando processamento de imagens...")
        novas_imagens_processadas = []
        for i in range(collectionSize):
            image = ee.Image(collectionList.get(i))
            image_id = image.getInfo()['id']
            if image_id not in lista_imagens_processadas:
                logging.getLogger('info_logger').info(repr(("Imagem nao processada: ", image_id)))
                image = ee.Image(image.getInfo()['id'])
                logging.getLogger('info_logger').info(repr(("Processando imagem: ", image.getInfo()['id'])))
                try:
                    self.processa_image(image, gyn)
                except Exception as e:
                    logging.getLogger('error_logger').error(repr(("Unexpected error:", sys.exc_info()[0])))
                    raise

                f = open(arquivo_lista_imagens_processadas, 'a')
                f.write(image.getInfo()['id'] + '\n')
                f.close()
                novas_imagens_processadas.append(image.getInfo()['id'])
            else:
                logging.getLogger('info_logger').info(repr(("Imagem JÁ processada: ", image_id)))

        logging.getLogger('info_logger').info("Processamento de imagens concluído!")
        if len(novas_imagens_processadas) > 0:
            logging.getLogger('info_logger').info(repr(("Processadas", len(novas_imagens_processadas), "novas imagens!")))
            pass

    def processa_image(self, image, gyn):

        logging.getLogger('info_logger').info("Iniciando processamento de imagem...")
        latlon = ee.Image.pixelLonLat().addBands(image)
        # apply reducer to list
        latlon = latlon.reduceRegion(
            reducer=ee.Reducer.toList(),
            geometry=gyn,
            maxPixels=1e8,
            scale=20);

        logging.getLogger('info_logger').info("Carregando valores de bandas...")
        # get data into different arrays
        start = datetime.now()

        keys = latlon.getInfo().keys()
        lats = np.array((ee.Array(latlon.get("latitude")).getInfo()))
        lons = np.array((ee.Array(latlon.get("longitude")).getInfo()))
        # b1s = np.array((ee.Array(latlon.get("B1")).getInfo()))
        b2s = np.array((ee.Array(latlon.get("B2")).getInfo()))
        b3s = np.array((ee.Array(latlon.get("B3")).getInfo()))
        b4s = np.array((ee.Array(latlon.get("B4")).getInfo()))
        # b5s = np.array((ee.Array(latlon.get("B5")).getInfo()))
        # b6s = np.array((ee.Array(latlon.get("B6")).getInfo()))
        # b7s = np.array((ee.Array(latlon.get("B7")).getInfo()))
        b8s = np.array((ee.Array(latlon.get("B8")).getInfo()))
        # b8As = np.array((ee.Array(latlon.get("B8A")).getInfo()))
        # b9s = np.array((ee.Array(latlon.get("B9")).getInfo()))
        # b10s = np.array((ee.Array(latlon.get("B10")).getInfo()))
        # b11s = np.array((ee.Array(latlon.get("B11")).getInfo()))
        # b12s = np.array((ee.Array(latlon.get("B12")).getInfo()))

        end = datetime.now()
        duracao = end - start
        duracao_str = str(duracao)
        logging.getLogger('info_logger').info(repr(("Bandas processadas em: ", duracao_str)))
        logging.getLogger('info_logger').info("Criando Dataframe...")
        ## Criando Pandas Dataframe com espectro das bandas.
        start_pandas = datetime.now()

        espectros = pd.DataFrame(data=[b2s, b3s, b4s, b8s, lats, lons])
        espectros = espectros.T
        colunas = ['B2', 'B3', 'B4', 'B8', 'LAT', 'LON']
        espectros.columns = colunas
        pd.unique(espectros[colunas].values.ravel('K'))

        # Now we will convert it into 'int' type.
        espectros.B2 = espectros.B2.astype('int')
        espectros.B3 = espectros.B3.astype('int')
        espectros.B4 = espectros.B4.astype('int')
        espectros.B8 = espectros.B8.astype('int')

        end_pandas = datetime.now()
        duracao_pandas = end_pandas - start_pandas
        duracao_str = str(duracao_pandas)
        logging.getLogger('info_logger').info(repr(("Dataframe criado em: ", duracao_str)))

        colunas_pred = ['B2', 'B3', 'B4', 'B8']

        start_predicao = datetime.now()

        precisao = 1

        logging.getLogger('info_logger').info(repr(("Carregando arquivo de parâmetros da IA: ", duracao_str)))
        try:
            finalized_model = 'optimus_modelo-2019.08.13.sav'
            loaded_model = joblib.load(finalized_model)
            logging.getLogger('info_logger').info(repr(("Arquivo de parâmetros carregado : ", finalized_model)))
        except:
            logging.getLogger('error_logger').error('Unexpected error ao carregar arquivo de parâmetros :', sys.exc_info()[0])
            raise

        logging.getLogger('info_logger').info(repr(("Iniciando predição de cada pixel")))
        predicao = []
        for keys, row in tqdm(espectros.iterrows(), total=espectros.shape[0]):
            coordenadas = [row['LAT'], row['LON']]
            to_predict = np.asarray([[int(row['B2']), int(row['B3']), int(row['B4']), int(row['B8'])]])
            predict_proba = loaded_model.predict_proba(to_predict)
            if predict_proba[0][0] >= precisao:
                # predicao.append([coordenadas, "Entulho"])
                predicao.append(
                    [coordenadas, "Entulho", int(row['B2']), int(row['B3']), int(row['B4']), int(row['B8'])])

        end_predicao = datetime.now()
        duracao_predicao = end_predicao - start_predicao
        duracao_str = str(duracao_predicao)
        logging.getLogger('info_logger').info(repr(("Predição concluída em: ", duracao_str)))
        logging.getLogger('info_logger').info(repr(("Processando METADADOS a imagem")))
        DT_PROC = datetime.now()
        df_predicao = pd.DataFrame(columns=['LAT-LON', 'PRED', 'B2', 'B3', 'B4', 'B8'])
        for p in tqdm(predicao):
            df_predicao = df_predicao.append(
                {'LAT-LON': p[0], 'PRED': str(p[1]), 'B2': int(p[2]), 'B3': int(p[3]), 'B4': int(p[4]),
                 'B8': int(p[5])}, ignore_index=True)

        IMAGE_INDEX = image.get('system:index').getInfo()
        IMAGE_ID = image.getInfo()['id']
        IMAGE_REFLECTANCE_CONVERSION_CORRECTION = image.get('REFLECTANCE_CONVERSION_CORRECTION').getInfo()
        IMAGE_CLOUDY_PIXEL_PERCENTAGE = image.get('CLOUDY_PIXEL_PERCENTAGE').getInfo()
        IMAGE_GENERATION_TIME = image.get('GENERATION_TIME').getInfo()
        IMAGE_SPACECRAFT_NAME = image.get('SPACECRAFT_NAME').getInfo()

        # Incluir informações da imagem que foi processada.
        df_predicao['IMAGE_INDEX'] = IMAGE_INDEX
        df_predicao['IMAGE_ID'] = IMAGE_ID
        df_predicao['IMAGE_REFLECTANCE_CONVERSION_CORRECTION'] = IMAGE_REFLECTANCE_CONVERSION_CORRECTION
        df_predicao['IMAGE_CLOUDY_PIXEL_PERCENTAGE'] = IMAGE_CLOUDY_PIXEL_PERCENTAGE
        df_predicao['IMAGE_GENERATION_TIME'] = IMAGE_GENERATION_TIME
        df_predicao['IMAGE_SPACECRAFT_NAME'] = IMAGE_SPACECRAFT_NAME

        dt_satelite = datetime.utcfromtimestamp(IMAGE_GENERATION_TIME / 1000).strftime('%Y-%m-%d')
        # export_filename_json = os.path.join('resultado', 'json', 'df_predicao_' + dt_satelite + '.json')
        # export_json = df_predicao.to_json(export_filename_json)

        df = df_predicao

        logging.getLogger('info_logger').info("Iniciando Cálculo de distância das áreas de proteção...")
        df['DISTANCIA_APA'] = df['LAT-LON'].apply(self.calc_distance_apa)
        logging.getLogger('info_logger').info("Cálculo de distância de APA concluído!")
        df['DISTANCIA_APP'] = df['LAT-LON'].apply(self.calc_distance_app)
        logging.getLogger('info_logger').info("Cálculo de distância de APP concluído!")
        df['DISTANCIA_ARA'] = df['LAT-LON'].apply(self.calc_distance_ara)
        logging.getLogger('info_logger').info("Cálculo de distância de ARA concluído!")
        logging.getLogger('info_logger').info("Cálculo de distância das áreas de proteção concluído!")

        logging.getLogger('info_logger').info("Preparando dados para envio ao backend-api/Frontend")
        df[['LAT', 'LON']] = pd.DataFrame(df['LAT-LON'].values.tolist(), index=df.index)
        df = df.drop(columns=['LAT-LON'])
        d = datetime.now()
        timezone = pytz.timezone("America/Sao_Paulo")
        d_aware = timezone.localize(d)
        df['data'] = d_aware
        colunas = ['descricao', 'band_b2', 'band_b3', 'band_b4', 'band_b8',
                   'image_index', 'imagemid', 'image_reflectance', 'image_cloudy_percentage',
                   'image_generation_time', 'image_spacecraft_name', 'distancia_apa', 'distancia_app', 'distancia_ara',
                   'latitude',
                   'longitude', 'data_processamento']
        df.columns = colunas
        file_csv = ''
        export_filename_csv = os.path.join('resultado', 'csv', 'distance_df_predicao_' + dt_satelite + '.csv')
        export_csv = df.to_csv(export_filename_csv)
        logging.getLogger('info_logger').info(repr(("Arquivo CSV exportado: ", export_filename_csv)))

        #  filename = os.path.join('resultado', 'csv', 'distance_df_predicao_2019-09-05.csv')
        #  df = pd.read_csv(filename)
        self.insert_data_to_database(df)

        logging.getLogger('info_logger').info("Processamento da imagem concluído!")

        return ''

    def calc_distance_apa(self, lat_lon):
        apa_shp_file = os.path.join('shp', 'apa.shp', 'APA.shp')
        lat, lon = lat_lon
        dist = Distance()
        distance = dist.menor_distancia_shape(lat, lon, apa_shp_file)
        return distance

    def calc_distance_app(self, lat_lon):
        app_shp_file = os.path.join('shp', 'app.shp', 'APP.shp')
        lat, lon = lat_lon
        dist = Distance()
        distance = dist.menor_distancia_shape(lat, lon, app_shp_file)
        return distance

    def calc_distance_ara(self, lat_lon):
        ara_shp_file = os.path.join('shp', 'ara.shp', 'ARA.shp')
        lat, lon = lat_lon
        dist = Distance()
        distance = dist.menor_distancia_shape(lat, lon, ara_shp_file)
        return distance

    def insert_data_to_database(self, df):
        logging.getLogger('info_logger').info("Iniciando processo de salvar em Banco de Dados!")

        colunas = ['descricao', 'band_b2', 'band_b3', 'band_b4', 'band_b8', 'image_index', 'imagemid',
                   'image_reflectance', 'image_cloudy_percentage', 'image_generation_time', 'image_spacecraft_name', 'distancia_apa',
                   'distancia_app', 'distancia_ara', 'data_processamento', 'coordenada']
        lista = []
        for z, row in df.iterrows():
            pnt = Point(row['longitude'], row['latitude'], srid=4326)
            lst = [row['descricao'], row['band_b2'], row['band_b3'], row['band_b4'], row['band_b8'], row['image_index'],
                 row['imagemid'], row['image_reflectance'], row['image_cloudy_percentage'], row['image_generation_time'], row['image_spacecraft_name'],
                 row['distancia_apa'],
                 row['distancia_app'],
                 row['distancia_ara'],
                 row['data_processamento'],
                 pnt]
            lista.append(lst)
        df_geo = pd.DataFrame(lista, columns=colunas)

        RCC.objects.bulk_create(
            RCC(**vals) for vals in df_geo.to_dict('records')
        )
        logging.getLogger('info_logger').info("Processo de salvar em Banco de Dados CONCLUÍDO!")
        return "insert_data_to_database CONCLUÍDO!"
