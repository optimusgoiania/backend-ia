from django.apps import AppConfig


class IntegraConfig(AppConfig):
    name = 'integra'
